#
#  This mkbuild is free software; you can redistribute it and/or modify it under the
#  terms of the GNU General Public License as published by the Free Software
#  Foundation; either version 2 of the License, or any later version.
#
#  This mkbuild is distributed in the hope that it will be useful, but WITHOUT ANY
#  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
#  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program; if not, write to the Free Software Foundation, Inc., 59 Temple
#  Place - Suite 330, Boston, MA 02111-1307, USA
#

#--------------------
# Personal variables
#--------------------
# Author name
[[SLACKBUILD AUTHOR]]="Silvio Rhatto"

# Initials author name
[[SLACKBUILD AUTHOR INITIALS]]="rha"

#------------------------
# Construction Variables
#------------------------
#
# Complete URL address or URL base address ( without $SRC_NAME-$VERSION... )
[[DOWNLOAD FOLDER URL]]="http://www.fftw.org/fftw-3.2.1.tar.gz"

#
# Other configure arguments
[[OTHER CONFIGURE ARGS]]="--enable-shared --enable-float"

#
# Documentation files. Null ("") to auto-set commom doc-files:
[[DOCUMENTATION FILES]]="AUTHORS CONVENTIONS COPYING COPYRIGHT ChangeLog NEWS README TODO"

# SlackBuild model
[[SLACKBUILD MODEL]]="generic.mkSlackBuild"

# SlackBuild PATH in Slack.Sarava tree
[[SLACKBUILD PATH]]="sci/libs/fftw"

#---------------------
# SlackBuild Sections
#---------------------
# Default sections:
#      head, set_variables, slkflags, start_structure, untar_source,
#      make_package, install_package, build_package
# Warning: don't remove '#>>' and "#<<" tags.
#>> Start SlackBuild Sections:
 on: head
 on: slackbuildrc
 on: set_variables
 on: slkflags
 on: error_codes
 on: start_structure
off: create_build_user_and_group
 on: download_source
off: md5sum_download_and_check_0
off: md5sum_download_and_check_1
off: gpg_signature_check
 on: manifest_check
 on: untar_source
 on: patch_source
off: configure
 on: make_package
off: install_package
 on: strip_binaries
off: compress_manpages
off: compress_info_files
off: copy_init_scripts
 on: install_documentation
 on: slackdesc
off: move_config_files
off: postinstall_script
 on: build_package
 on: clean_builds
#<< End SlackBuild Sections

#------------------
# Sections changes
#------------------
#>slackdesc
fftw: fftw (the fastest fourier transform in the west)
fftw:
fftw: FFTW is a C subroutine library for computing the discrete Fourier
fftw: transform (DFT) in one or more dimensions, of arbitrary input size,
fftw: and of both real and complex data (as well as of even/odd data, i.e.
fftw: the discrete cosine/sine transforms or DCT/DST). We believe that FFTW,
fftw: which is free software, should become the FFT library of choice for
fftw: most applications.
fftw:
fftw:
fftw:
#<slackdesc

#>make_package
# Thanks to Kyle Guinn <elyk at 3@gmail.com> for the section below,
# obtained from http://slackbuilds.org/slackbuilds/12.1/libraries/fftw/fftw.SlackBuild
# Small changes by Rhatto

# compile fftw3
CFLAGS= \
CXXFLAGS= \
FFLAGS= \
./configure \
  --prefix=/usr \
  --sysconfdir=/etc \
  --libdir="$LIBDIR" \
  --localstatedir=/var \
  --mandir=/usr/man \
  --infodir=/usr/info \
  --docdir=/usr/doc/fftw-$PKG_VERSION \
  --enable-shared \
  --disable-static \
  --enable-threads || exit $ERROR_CONF
make $NUMJOBS || exit $ERROR_MAKE
make install-strip DESTDIR="$PKG" || exit $ERROR_INSTALL
make clean || exit $ERROR_MAKE

# compile fftw3f
CFLAGS= \
CXXFLAGS= \
FFLAGS= \
./configure \
  --prefix=/usr \
  --sysconfdir=/etc \
  --libdir="$LIBDIR" \
  --localstatedir=/var \
  --mandir=/usr/man \
  --infodir=/usr/info \
  --docdir=/usr/doc/fftw-$PKG_VERSION \
  --enable-shared \
  --disable-static \
  --enable-threads \
  --enable-float || exit $ERROR_CONF
make $NUMJOBS || exit $ERROR_MAKE
make install-strip DESTDIR="$PKG" || exit $ERROR_INSTALL
make clean || exit $ERROR_MAKE

# compile fftw3l
CFLAGS= \
CXXFLAGS= \
FFLAGS= \
./configure \
  --prefix=/usr \
  --sysconfdir=/etc \
  --libdir="$LIBDIR" \
  --localstatedir=/var \
  --mandir=/usr/man \
  --infodir=/usr/info \
  --docdir=/usr/doc/fftw-$PKG_VERSION \
  --enable-shared \
  --disable-static \
  --enable-threads \
  --enable-long-double || exit $ERROR_CONF
make $NUMJOBS || exit $ERROR_MAKE
make install-strip DESTDIR="$PKG" || exit $ERROR_INSTALL
#<make_package
