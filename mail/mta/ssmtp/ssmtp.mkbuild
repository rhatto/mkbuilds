#
#  This mkbuild is free software; you can redistribute it and/or modify it under the
#  terms of the GNU General Public License as published by the Free Software
#  Foundation; either version 2 of the License, or any later version.
#
#  This mkbuild is distributed in the hope that it will be useful, but WITHOUT ANY
#  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
#  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program; if not, write to the Free Software Foundation, Inc., 59 Temple
#  Place - Suite 330, Boston, MA 02111-1307, USA
#
# Version 0.9.0 - Alves ( rudsonalves at yahoo d0t com d0t br )
# Version $Rev: 742 $ - $Author: rhatto $

#--------------------
# Personal variables
#--------------------
# Author name
[[SLACKBUILD AUTHOR]]="Silvio Rhatto"

# Initials author name
[[SLACKBUILD AUTHOR INITIALS]]="rha"

#------------------------
# Construction Variables
#------------------------
#
# Complete URL address or URL base address ( without $SRC_NAME-$VERSION... )
[[DOWNLOAD FOLDER URL]]="http://ftp.de.debian.org/debian/pool/main/s/ssmtp/ssmtp_2.61.orig.tar.gz"

# Source base name. if different from package name. Null ("") to default value.
# Auto-set, get SRC_NAME from URL: http://.../$SRC_NAME-$VERSION.tar.$EXTENSION'
[[SOURCE NAME]]="ssmtp"

#
# Package version.  Null ("") to auto-set.
# Auto-set, get VERSION from URL: http://.../$SRC_NAME-$VERSION.tar.$EXTENSION'
[[VERSION]]="6.61"

#
# Source Name construction string
# Default value is: $SRC_NAME-$VERSION.tar.$EXTENSION
[[SOURCE NAME CONSTRUCTION STRING]]="$SRC_NAME"_"$VERSION.orig.tar.$EXTENSION"

#
# Documentation files. Null ("") to auto-set commom doc-files:
# NEWS TODO README AUTHORS INSTALL ChangeLog MAINTAINERS COPYING readme.*
[[DOCUMENTATION FILES]]="CHANGELOG_OLD COPYING INSTALL README TLS"

# Install script
[[REST OF DOINST.SH]]="config etc/ssmtp/ssmtp.conf.new ; config etc/ssmtp/revaliases.conf.new ; ( cd usr/man/man8 ; rm -rf sendmail.8.gz ) ; ( cd usr/man/man8 ; ln -sf .gz sendmail.8.gz ) ; ( cd usr/sbin ; rm -rf sendmail ) ; ( cd usr/sbin ; ln -sf ssmtp sendmail ) ; ( cd usr/lib ; ln -sf ../sbin/ssmtp sendmail )"

# SlackBuild model
[[SLACKBUILD MODEL]]="generic.mkSlackBuild"

# SlackBuild PATH in Slack.Sarava tree
#[[SLACKBUILD PATH]]="others/unclassified/ssmtp"
[[SLACKBUILD PATH]]="mail/mta/ssmtp"

#---------------------
# SlackBuild Sections
#---------------------
# Default sections:
#      head, set_variables, slkflags, start_structure, untar_source,
#      make_package, install_package, build_package
# Warning: don't remove '#>>' and "#<<" tags.
#>> Start SlackBuild Sections:
 on: head
 on: slackbuildrc
 on: set_variables
 on: slkflags
 on: error_codes
 on: start_structure
off: create_build_user_and_group
 on: download_source
off: download_patches
off: svn_source
off: git_source
off: md5sum_download_and_check_0
off: md5sum_download_and_check_1
off: gpg_signature_check
 on: manifest_check
 on: untar_source
off: patch_source
 on: configure
 on: make_package
 on: install_package
 on: strip_binaries
off: compress_manpages
off: compress_info_files
off: copy_init_scripts
off: copy_config_files
 on: install_documentation
 on: slackdesc
off: move_config_files
 on: postinstall_script
off: slack_required
 on: build_package
 on: clean_builds
#<< End SlackBuild Sections

#------------------
# Sections changes
#------------------
#>slackdesc
ssmtp: ssmtp (extremely simple MTA)
ssmtp:
ssmtp: This is sSMTP, a program that replaces sendmail on workstations
ssmtp: that should send their mail via the departmental mailhub from which
ssmtp: they pick up their mail (via pop, imap, rsmtp, pop_fetch, NFS... or
ssmtp: the like).  This program accepts mail and sends it to the mailhub,
ssmtp: optionally replacing the domain in the From: line with a different
ssmtp: one.
ssmtp:
ssmtp:
ssmtp:
#<slackdesc

#>install_package
# Install
mkdir -p $PKG/usr/{sbin,man/man8}
mkdir -p $PKG/etc/ssmtp
cp ssmtp $PKG/usr/sbin ; chmod 755 $PKG/usr/sbin/ssmtp
cp ssmtp.8 $PKG/usr/man/man8 ; chmod 644 $PKG/usr/man/man8/ssmtp.8
cp ssmtp.conf $PKG/etc/ssmtp/ssmtp.conf.new
cp revaliases $PKG/etc/ssmtp/revaliases.conf.new
#<install_package
