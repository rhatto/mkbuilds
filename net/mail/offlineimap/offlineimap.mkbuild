#
#  This mkbuild is free software; you can redistribute it and/or modify it under the
#  terms of the GNU General Public License as published by the Free Software
#  Foundation; either version 2 of the License, or any later version.
#
#  This mkbuild is distributed in the hope that it will be useful, but WITHOUT ANY
#  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
#  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program; if not, write to the Free Software Foundation, Inc., 59 Temple
#  Place - Suite 330, Boston, MA 02111-1307, USA
#

#--------------------
# Personal variables
#--------------------
# Author name
[[SLACKBUILD AUTHOR]]="Silvio Rhatto"

# Initials author name
[[SLACKBUILD AUTHOR INITIALS]]="rha"

#------------------------
# Construction Variables
#------------------------
#
# Complete URL address or URL base address ( without $SRC_NAME-$VERSION... )
[[DOWNLOAD FOLDER URL]]="http://ftp.de.debian.org/debian/pool/main/o/offlineimap/offlineimap_6.0.3.tar.gz"

# Source base name
[[SOURCE NAME]]="offlineimap"

# Package name
[[PACKAGE NAME]]="offlineimap"

#
# Package version
[[VERSION]]="6.0.3"

#
# Source Name construction string
[[SOURCE NAME CONSTRUCTION STRING]]="$SRC_NAME"_"$SRC_VERSION.tar.$EXTENSION"

#
# Documentation files
[[DOCUMENTATION FILES]]="COPYING COPYRIGHT FAQ.html README UPGRADING"

# SlackBuild model
[[SLACKBUILD MODEL]]="generic.mkSlackBuild"

# SlackBuild PATH in Slack.Sarava tree
[[SLACKBUILD PATH]]="net/mail/offlineimap"

#---------------------
# SlackBuild Sections
#---------------------
# Default sections:
#      head, set_variables, slkflags, start_structure, untar_source,
#      make_package, install_package, build_package
# Warning: don't remove '#>>' and "#<<" tags.
#>> Start SlackBuild Sections:
 on: head
 on: slackbuildrc
 on: set_variables
 on: slkflags
 on: error_codes
 on: start_structure
off: create_build_user_and_group
 on: download_source
off: md5sum_download_and_check_0
off: md5sum_download_and_check_1
off: gpg_signature_check
 on: manifest_check
 on: untar_source
 on: patch_source
off: configure
 on: make_package
off: install_package
 on: strip_binaries
off: compress_manpages
off: compress_info_files
off: copy_init_scripts
 on: install_documentation
 on: slackdesc
off: move_config_files
off: postinstall_script
 on: build_package
 on: clean_builds
#<< End SlackBuild Sections

#------------------
# Sections changes
#------------------
#>slackdesc
offlineimap: offlineimap
offlineimap:
offlineimap: OfflineIMAP is a tool to simplify your e-mail reading.
offlineimap: With OfflineIMAP, you can read the same mailbox from multiple
offlineimap: computers. You get a current copy of your messages on each computer,
offlineimap: and changes you make one place will be visible on all other systems.
offlineimap: For instance, you can delete a message on your home computer, and it
offlineimap: will appear deleted on your work computer as well. OfflineIMAP is
offlineimap: also useful if you want to use a mail reader that does not have IMAP
offlineimap: support, has poor IMAP support, or does not provide disconnected
offlineimap: operation.
#<slackdesc

#>make_package
# Build and install package
python setup.py build install --root=$PKG
#<make_package
