#
#  This mkbuild is free software; you can redistribute it and/or modify it under the
#  terms of the GNU General Public License as published by the Free Software
#  Foundation; either version 2 of the License, or any later version.
#
#  This mkbuild is distributed in the hope that it will be useful, but WITHOUT ANY
#  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
#  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program; if not, write to the Free Software Foundation, Inc., 59 Temple
#  Place - Suite 330, Boston, MA 02111-1307, USA
#

#--------------------
# Personal variables
#--------------------
# Author name
[[SLACKBUILD AUTHOR]]="Silvio Rhatto"

# Initials author name
[[SLACKBUILD AUTHOR INITIALS]]="rha"

#------------------------
# Construction Variables
#------------------------
#
# Complete URL address or URL base address ( without $SRC_NAME-$VERSION... )
[[DOWNLOAD FOLDER URL]]="http://download.videolan.org/pub/videolan/vlc/1.0.2/vlc-1.0.2.tar.bz2"

#
# Build number.
# Auto-set to default 1
[[BUILD NUMBER]]=""

#
# Dependency list input
[[SLACK REQUIRED]]="jack libavc1394: libraw1394: libdc1394: mpeg2dec: liba52: faac: lame: wxWidgets: libshout: ffmpeg: qt4"

#
# Other configure arguments
#
# - Using --disable-dc1394 is a temporary workaround; please substitute
#   by --enable-dc1394 when possible.
#
[[OTHER CONFIGURE ARGS]]="--enable-theora --enable-dv --enable-v4l --enable-real --enable-realrtsp --enable-flac --enable-snapshot --enable-aa --enable-jack --enable-shout --disable-dc1394 --disable-nls --disable-mozilla"

#
# Documentation files. Null ("") to auto-set commom doc-files:
[[DOCUMENTATION FILES]]="AUTHORS COPYING ChangeLog NEWS README TODO"

# SlackBuild model
[[SLACKBUILD MODEL]]="generic.mkSlackBuild"

# SlackBuild PATH in Slack.Sarava tree
[[SLACKBUILD PATH]]="media/video/vlc"

#---------------------
# SlackBuild Sections
#---------------------
# Default sections:
#      head, set_variables, slkflags, start_structure, untar_source,
#      make_package, install_package, build_package
# Warning: don't remove '#>>' and "#<<" tags.
#>> Start SlackBuild Sections:
 on: head
 on: slackbuildrc
 on: set_variables
 on: slkflags
 on: error_codes
 on: start_structure
off: create_build_user_and_group
 on: download_source
off: md5sum_download_and_check_0
off: md5sum_download_and_check_1
off: gpg_signature_check
 on: manifest_check
 on: untar_source
 on: patch_source
 on: configure
 on: make_package
 on: install_package
 on: strip_binaries
off: compress_manpages
off: compress_info_files
off: copy_init_scripts
 on: install_documentation
 on: slackdesc
off: move_config_files
off: postinstall_script
 on: slack_required
 on: build_package
 on: clean_builds
#<< End SlackBuild Sections

#------------------
# Sections changes
#------------------
#>slackdesc
vlc: VLC (Video Lan Client)
vlc:
vlc: VLC media player is a highly portable multimedia player for various 
vlc: audio and video formats (MPEG-1, MPEG-2, MPEG-4, DivX, mp3, ogg, ...)
vlc: as well as DVDs, VCDs, and various streaming protocols. It can also 
vlc: be used as a server to stream in unicast or multicast in IPv4 or IPv6
vlc: on a high-bandwidth network.
vlc:
vlc:
vlc: http://www.videolan.org/
vlc:
#<slackdesc
