#
#  This mkbuild is free software; you can redistribute it and/or modify it under the
#  terms of the GNU General Public License as published by the Free Software
#  Foundation; either version 2 of the License, or any later version.
#
#  This mkbuild is distributed in the hope that it will be useful, but WITHOUT ANY
#  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
#  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program; if not, write to the Free Software Foundation, Inc., 59 Temple
#  Place - Suite 330, Boston, MA 02111-1307, USA
#

#--------------------
# Personal variables
#--------------------
# Author name
[[SLACKBUILD AUTHOR]]="Silvio Rhatto"

# Initials author name
[[SLACKBUILD AUTHOR INITIALS]]="rha"

#------------------------
# Construction Variables
#------------------------
#
# Complete URL address or URL base address ( without $SRC_NAME-$VERSION... )
[[DOWNLOAD FOLDER URL]]="http://ardour.org/files/releases/ardour-2.7.1.tar.bz2"

#
# Documentation files. Null ("") to auto-set commom doc-files:
[[DOCUMENTATION FILES]]="AUTHORS AUTHORS.es BUILD CONTRIBUTORS FAQ README TRANSLATORS"

# SlackBuild model
[[SLACKBUILD MODEL]]="generic.mkSlackBuild"

# SlackBuild PATH in Slack.Sarava tree
[[SLACKBUILD PATH]]="media/sound/ardour"

# Dependencies
[[SLACK REQUIRED]]="scons: raptor: liblrdf: ladspa: libsamplerate: libsndfile: jack: libgnomecanvas: liblo: fftw: boost: aubio"

#---------------------
# SlackBuild Sections
#---------------------
# Default sections:
#      head, set_variables, slkflags, start_structure, untar_source,
#      make_package, install_package, build_package
# Warning: don't remove '#>>' and "#<<" tags.
#>> Start SlackBuild Sections:
 on: head
 on: slackbuildrc
 on: set_variables
 on: slkflags
 on: error_codes
 on: start_structure
off: create_build_user_and_group
 on: download_source
off: md5sum_download_and_check_0
off: md5sum_download_and_check_1
off: gpg_signature_check
 on: manifest_check
 on: untar_source
 on: patch_source
off: configure
 on: make_package
 on: install_package
 on: strip_binaries
off: compress_manpages
off: compress_info_files
off: copy_init_scripts
 on: install_documentation
 on: slackdesc
off: move_config_files
off: postinstall_script
 on: slack_required
 on: build_package
 on: clean_builds
#<< End SlackBuild Sections

#------------------
# Sections changes
#------------------
#>slackdesc
ardour: ardour (Digital Audio Workstation)
ardour:
ardour: Ardour is a professional multitrack/multichannel audio recorder and
ardour: DAW for Linux, using ALSA-supported audio interfaces. It supports
ardour: up to 32 bit samples, 24+ channels at up to 96kHz, full MMC control,
ardour: a non-destructive, non-linear editor, and LADSPA plugins.
ardour:
ardour:
ardour:
ardour:
ardour:
#<slackdesc

#>make_package
# Compile

# This section and the install section uses code from
# http://slackbuilds.org/slackbuilds/12.1/audio/ardour/ardour.SlackBuild,
# which licence is:

# Copyright 2008 Heinz Wiesinger <hmwiesinger@gmx.at>
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
# EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

scons PREFIX=/usr \
  DIST_TARGET="none" \
  ARCH="$(echo $SLKCFLAGS)" \
  FREEDESKTOP=1 || exit $ERROR_MAKE
#<make_package

#>install_package
# Install
scons \
  PREFIX=/usr \
  DIST_TARGET="none" \
  ARCH="$(echo $SLKCFLAGS)" \
  FREEDESKTOP=0 \
  DESTDIR=$PKG \
  install || exit $ERROR_INSTALL

# We'll have to install the freedesktop stuff manually
# See the FREEDESKTOP target in $srcdir/gtk2_ardour/Sconscript
# to be sure this is synced with future releases
cd gtk2_ardour
  # First, install the icons
  mkdir -p $PKG/usr/share/icons/hicolor/{16x16,22x22,32x32,48x48}/apps
  install -m 0644 icons/ardour_icon_16px.png \
    $PKG/usr/share/icons/hicolor/16x16/apps/ardour2.png || exit $ERROR_INSTALL
  install -m 0644 icons/ardour_icon_22px.png \
    $PKG/usr/share/icons/hicolor/22x22/apps/ardour2.png || exit $ERROR_INSTALL
  install -m 0644 icons/ardour_icon_32px.png \
    $PKG/usr/share/icons/hicolor/32x32/apps/ardour2.png || exit $ERROR_INSTALL
  install -m 0644 icons/ardour_icon_48px.png \
    $PKG/usr/share/icons/hicolor/48x48/apps/ardour2.png || exit $ERROR_INSTALL
  # Next, install the mime xml file
  mkdir -p $PKG/usr/share/mime/packages
  install -m 0644 ardour2.xml $PKG/usr/share/mime/packages || exit $ERROR_INSTALL
  # Now install the mimetype icons
  mkdir -p $PKG/usr/share/icons/hicolor/{16x16,22x22,32x32,48x48}/mimetypes
  install -m 0644 icons/application-x-ardour_16px.png \
    $PKG/usr/share/icons/hicolor/16x16/mimetypes/application-x-ardour2.png || exit $ERROR_INSTALL
  install -m 0644 icons/application-x-ardour_22px.png \
    $PKG/usr/share/icons/hicolor/22x22/mimetypes/application-x-ardour2.png || exit $ERROR_INSTALL
  install -m 0644 icons/application-x-ardour_32px.png \
    $PKG/usr/share/icons/hicolor/32x32/mimetypes/application-x-ardour2.png || exit $ERROR_INSTALL
  install -m 0644 icons/application-x-ardour_48px.png \
    $PKG/usr/share/icons/hicolor/48x48/mimetypes/application-x-ardour2.png || exit $ERROR_INSTALL
  # Finally, install the desktop file
  mkdir -p $PKG/usr/share/applications
  install -m 0644 ardour2.desktop.in \
    $PKG/usr/share/applications/ardour2.desktop || exit $ERROR_INSTALL
cd -
#<install_package
