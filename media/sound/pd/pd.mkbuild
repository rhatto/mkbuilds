#
#  This mkbuild is free software; you can redistribute it and/or modify it under the
#  terms of the GNU General Public License as published by the Free Software
#  Foundation; either version 2 of the License, or any later version.
#
#  This mkbuild is distributed in the hope that it will be useful, but WITHOUT ANY
#  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
#  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program; if not, write to the Free Software Foundation, Inc., 59 Temple
#  Place - Suite 330, Boston, MA 02111-1307, USA
#

#--------------------
# Personal variables
#--------------------
# Author name
[[SLACKBUILD AUTHOR]]="Silvio Rhatto"

# Initials author name
[[SLACKBUILD AUTHOR INITIALS]]="rha"

#------------------------
# Construction Variables
#------------------------
#
# Complete URL address or URL base address ( without $SRC_NAME-$VERSION... )
[[DOWNLOAD FOLDER URL]]="http://www-crca.ucsd.edu/~msp/Software/pd-0.41-4.src.tar.gz"

# Source base name. if different from package name. Null ("") to default value.
# Auto-set, get SRC_NAME from URL: http://.../$SRC_NAME-$VERSION.tar.$EXTENSION'
[[SOURCE NAME]]="pd"

#
# Package version.
[[VERSION]]="0.41-4"

#
# Source Name construction string
[[SOURCE NAME CONSTRUCTION STRING]]="$SRC_NAME-$VERSION.src.tar.$EXTENSION"

#
# Dependency list input
[[SLACK REQUIRED]]="jack"

#
# Other configure arguments 
[[OTHER CONFIGURE ARGS]]="--enable-alsa --enable-jack --enable-portaudio --enable-portmidi --enable-fftw"

#
# Documentation files. Null ("") to auto-set commom doc-files:
[[DOCUMENTATION FILES]]="LICENSE.txt README.txt"

# SlackBuild model
[[SLACKBUILD MODEL]]="generic.mkSlackBuild"

# SlackBuild PATH in Slack.Sarava tree
[[SLACKBUILD PATH]]="media/sound/pd"

# Patching
[[NUMBER OF PREFIX SLASHES TO STRIP]]="2"

#---------------------
# SlackBuild Sections
#---------------------
# Default sections:
#      head, set_variables, slkflags, start_structure, untar_source,
#      make_package, install_package, build_package
# Warning: don't remove '#>>' and "#<<" tags.
#>> Start SlackBuild Sections:
 on: head
 on: slackbuildrc
 on: set_variables
 on: slkflags
 on: error_codes
 on: start_structure
off: create_build_user_and_group
 on: download_source
off: md5sum_download_and_check_0
off: md5sum_download_and_check_1
off: gpg_signature_check
 on: manifest_check
 on: untar_source
 on: patch_source
 on: configure
 on: make_package
 on: install_package
 on: strip_binaries
off: compress_manpages
off: compress_info_files
off: copy_init_scripts
 on: install_documentation
 on: slackdesc
off: move_config_files
off: postinstall_script
 on: slack_required
 on: build_package
 on: clean_builds
#<< End SlackBuild Sections

#------------------
# Sections changes
#------------------
#>slackdesc
pd: pd (Pure Data)
pd:
pd: PD (Pure Data) is a real-time graphical programming environment for audio,
pd: video, and graphical processing. It is the third major branch of the family
pd: of patcher programming languages known as Max (Max/FTS, ISPW Max, Max/MSP,
pd: jMax, etc.).
pd:
pd:
pd:
pd:
pd:
#<slackdesc

#>make_package
# Compile
make depend || exit $ERROR_MAKE
make $NUMJOBS || exit $ERROR_MAKE
#<make_package

#p>
-cd "$PKG_SRC"
+cd "$PKG_SRC/src"
#p<
